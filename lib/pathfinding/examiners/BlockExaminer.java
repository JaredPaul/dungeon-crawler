package me.jared.dungeoncrawler.pathfinding.examiners;

import me.jared.dungeoncrawler.pathfinding.sources.BlockSource;
import org.bukkit.Location;

/**
 * Created by JPaul on 6/20/2016.
 */
public interface BlockExaminer
{
    float getCost(BlockSource blockSource, VectorNode node);

    PassableState isPassable(BlockSource blockSource, VectorNode node, Location start, Location end);

    enum PassableState
    {
        IGNORE,
        PASSABLE,
        UNPASSABLE;

        public static PassableState fromBoolean(boolean flag)
        {
            return flag ? PASSABLE : UNPASSABLE;
        }
    }
}
