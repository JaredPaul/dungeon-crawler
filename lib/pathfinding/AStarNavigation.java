package me.jared.dungeoncrawler.pathfinding;

import me.jared.dungeoncrawler.pathfinding.examiners.MinecraftBlockExaminer;
import me.jared.dungeoncrawler.pathfinding.sources.ChunkBlockSource;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

import java.util.List;

/**
 * Created by JPaul on 6/22/2016.
 */
public class AStarNavigation
{
    private final Location destination;
    private final Location start;

    private final VectorGoal goal;
    private final VectorNode startNode;

    private Path path;
    private Vector nextDest;

    private final AStarMachine ASTAR = AStarMachine.createWithDefaultStorage();

    public AStarNavigation(Location start, Location destination)
    {
        this.start = start;
        this.destination = destination;

        this.goal = new VectorGoal(destination, 1);
        this.startNode = new VectorNode(start, destination, goal, start, new ChunkBlockSource(start, 64), new MinecraftBlockExaminer());
    }

    public void createPath(World world, LocationCallback locationCallback)
    {
        if (path != null)
            locationCallback.onSuccess(path.getPathLocations(world));


        path = ASTAR.runFully(goal, startNode, 500000);

        locationCallback.onSuccess(path.getPathLocations(world));
    }

    public Location getDestination()
    {
        return destination;
    }

    public void stop()
    {
        path = null;
    }

    public interface LocationCallback
    {
        void onSuccess(List<Location> locations);
    }
}
