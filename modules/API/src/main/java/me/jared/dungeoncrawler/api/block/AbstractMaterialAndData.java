package me.jared.dungeoncrawler.api.block;

import org.bukkit.Material;

public abstract class AbstractMaterialAndData implements IMaterialAndData
{
    protected Material material;

    public AbstractMaterialAndData(Material material)
    {
        this.material = material;
    }

    @Override
    public Material getMaterial()
    {
        return material;
    }
}
