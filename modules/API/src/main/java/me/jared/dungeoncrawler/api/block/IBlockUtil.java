package me.jared.dungeoncrawler.api.block;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.material.MaterialData;

import java.util.List;

public interface IBlockUtil
{
    boolean setBlockFast(Location location, MaterialData data);

    boolean setBlockFast(Location location, Material material, byte legacyData);

    boolean setBlockFast(Location location, IMaterialAndData materialAndData);

    void refreshChunkLocations(List<Location> locations);

    void refreshChunks(List<Chunk> chunks);

    List<Chunk> getChunks(List<Location> locations);
}
