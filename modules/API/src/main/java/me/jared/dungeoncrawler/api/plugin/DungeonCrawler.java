package me.jared.dungeoncrawler.api.plugin;

import me.jared.dungeoncrawler.api.block.IBlockUtil;
import me.jared.dungeoncrawler.api.logging.Log;
import me.jared.dungeoncrawler.api.registry.decorations.IDecorationRegistry;
import me.jared.dungeoncrawler.api.structures.IStructureUtil;

public class DungeonCrawler
{
    private static IDungeonCrawlerMain PLUGIN;
    public static final Log LOG = new Log("[DungeonCrawler]");

    public static void setPlugin(IDungeonCrawlerMain plugin)
    {
        if (PLUGIN != null)
            return;

        PLUGIN = plugin;
    }

    public static IDungeonCrawlerMain getPlugin()
    {
        return PLUGIN;
    }

    public static IDecorationRegistry getDecorationRegistry()
    {
        return PLUGIN.getDecorationRegistry();
    }

    public static IStructureUtil getStructureUtil()
    {
        return PLUGIN.getStructureUtil();
    }

    public static IBlockUtil getBlockUtil()
    {
        return PLUGIN.getBlockUtil();
    }
}
