package me.jared.dungeoncrawler.api.block;

import org.bukkit.Material;

public interface IMaterialAndData
{
    Material getMaterial();

    String getBlockData();

    byte getBlockDataLegacy();
}
