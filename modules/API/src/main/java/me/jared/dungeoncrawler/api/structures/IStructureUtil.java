package me.jared.dungeoncrawler.api.structures;

import org.bukkit.Location;

import java.io.File;
import java.io.IOException;

public interface IStructureUtil
{
    IStructure createStructure(Location[] corners, String author);

    void saveStructure(IStructure definedStructure, File destination) throws IOException;

    IStructure loadStructure(File source) throws IOException;

    void pasteStructure(IStructure definedStructure, Location startEdge, Object enumBlockRotation);

    int[] getDimensions(Location[] corners);

    Location[] normalizeEdges(Location startBlock, Location endBlock);
}
