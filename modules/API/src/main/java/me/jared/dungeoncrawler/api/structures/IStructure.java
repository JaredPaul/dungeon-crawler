package me.jared.dungeoncrawler.api.structures;

import me.jared.dungeoncrawler.api.block.IMaterialAndData;
import org.bukkit.Location;
import org.bukkit.util.Vector;

import java.io.File;
import java.util.List;
import java.util.Map;

public interface IStructure extends Cloneable
{
    String getName();

    void setName(String name);

    File getFile();

    void setFile(File file);

    AABB getTestAABB(Vector position, int angle);

    int getVolume();

    int getWidth();

    int getHeight();

    int getDepth();

    int[] getDimensions();

    void place(Location base);

    //returns a COPY of the block data based off the rotation angle
    void rotate(int angle);

    List<Vector> getRotatedEdgeCases(Vector base, int angle);

    Map<Vector, IMaterialAndData> getBlockMap();

    List<Vector> getPoints(Vector testPoint);

    IStructure clone();
}
