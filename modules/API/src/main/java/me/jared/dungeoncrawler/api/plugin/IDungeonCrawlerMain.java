package me.jared.dungeoncrawler.api.plugin;

import me.jared.dungeoncrawler.api.block.IBlockUtil;
import me.jared.dungeoncrawler.api.registry.decorations.IDecorationRegistry;
import me.jared.dungeoncrawler.api.structures.IStructureUtil;
import org.bukkit.plugin.Plugin;

public interface IDungeonCrawlerMain extends Plugin
{
    IDecorationRegistry getDecorationRegistry();

    IStructureUtil getStructureUtil();

    IBlockUtil getBlockUtil();
}
