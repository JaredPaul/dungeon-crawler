package me.jared.dungeoncrawler.api.structures;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import me.jared.dungeoncrawler.api.block.IMaterialAndData;
import me.jared.dungeoncrawler.api.plugin.DungeonCrawler;
import me.jared.dungeoncrawler.api.util.VectorUtil;
import org.bukkit.Location;
import org.bukkit.util.Vector;

import java.io.File;
import java.util.List;
import java.util.Map;

public abstract class AbstractStructure implements IStructure
{
    private String name;
    protected File file;
    protected int[] dimensions;
    protected Map<Vector, IMaterialAndData> blockMap = Maps.newHashMap();
    protected List<Vector> edgeCases = Lists.newArrayList();

    @Override
    public String getName()
    {
        return name;
    }

    @Override
    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public File getFile()
    {
        return file;
    }

    @Override
    public void setFile(File file)
    {
        this.file = file;
    }

    @Override
    public int getVolume()
    {
        int width = dimensions[0];
        int height = dimensions[1];
        int depth = dimensions[2];
        return width * height * depth;
    }

    @Override
    public AABB getTestAABB(Vector position, int angle)
    {
        int maxX = getWidth() - 1;
        int maxZ = getDepth() - 1;
        int maxY = getHeight() - 1;

        AABB aabb = new AABB(0, 0, 0, maxX, maxY, maxZ);

        return aabb.rotateAlongY(position, angle);
    }

    @Override
    public int getWidth()
    {
        return dimensions[0];
    }

    @Override
    public int getHeight()
    {
        return dimensions[1];
    }

    @Override
    public int getDepth()
    {
        return dimensions[2];
    }

    @Override
    public int[] getDimensions()
    {
        return dimensions;
    }

    @Override
    public void place(Location base)
    {
        for (Map.Entry<Vector, IMaterialAndData> blockEntry : blockMap.entrySet())
        {
            Vector position = blockEntry.getKey();
            IMaterialAndData materialAndData = blockEntry.getValue();

            Location location = base.clone().add(position);

            DungeonCrawler.getBlockUtil().setBlockFast(location, materialAndData);
        }
    }

    @Override
    public List<Vector> getRotatedEdgeCases(Vector base, int angle)
    {
        List<Vector> newEdges = Lists.newArrayList();

        for (Vector edgeCase : edgeCases)
        {
            Vector newEdge = VectorUtil.rotateVector(edgeCase, angle);
            newEdges.add(base.clone().add(newEdge));
        }

        return newEdges;
    }

    @Override
    public Map<Vector, IMaterialAndData> getBlockMap()
    {
        return blockMap;
    }

    @Override
    public List<Vector> getPoints(Vector testPoint)
    {
        List<Vector> testPoints = Lists.newArrayList();

        for (Vector point : blockMap.keySet())
        {
            testPoints.add(testPoint.clone().add(point));
        }

        return testPoints;
    }

    @Override
    public IStructure clone()
    {
        try
        {
            return (IStructure) super.clone();
        } catch (CloneNotSupportedException e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
