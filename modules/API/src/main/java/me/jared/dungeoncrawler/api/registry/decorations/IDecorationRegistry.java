package me.jared.dungeoncrawler.api.registry.decorations;

import me.jared.dungeoncrawler.api.cell.ICell;
import me.jared.dungeoncrawler.api.decorations.IDecoration;

import java.io.IOException;
import java.util.List;

public interface IDecorationRegistry
{
    List<IDecoration> getDecorations();

    IDecoration getDecoration(String name);

    IDecoration getRandomDecoration(ICell cell, IDecoration.Size size);

    void loadDecorations() throws IOException;
}
