package me.jared.dungeoncrawler.api.util;

import org.bukkit.block.BlockFace;
import org.bukkit.util.Vector;

public enum Direction
{
    EAST(1, 0, 0, BlockFace.EAST),
    WEST(-1, 0, 0, BlockFace.WEST),
    NORTH(0, 0, -1, BlockFace.NORTH),
    SOUTH(0, 0, 1, BlockFace.SOUTH),
    NORTH_EAST(NORTH, EAST),
    NORTH_WEST(NORTH, WEST),
    SOUTH_EAST(SOUTH, EAST),
    SOUTH_WEST(SOUTH, WEST),;

    public double xOffset, yOffset, zOffset;
    public BlockFace blockFace;

    Direction(double x, double y, double z, BlockFace blockFace)
    {
        this.xOffset = x;
        this.yOffset = y;
        this.zOffset = z;
        this.blockFace = blockFace;
    }

    Direction(Direction direction1, Direction direction2)
    {
        this.xOffset = direction1.xOffset + direction2.xOffset;
        this.yOffset = direction1.yOffset + direction2.yOffset;
        this.zOffset = direction1.zOffset + direction2.zOffset;
    }

    public static Direction[] getMainDirections()
    {
        return new Direction[] {EAST, WEST, NORTH, SOUTH};
    }

    public Vector toVector()
    {
        return new Vector(xOffset, yOffset, zOffset);
    }

    public BlockFace toBlockFace()
    {
        return blockFace;
    }
}
