package me.jared.dungeoncrawler.api.registry;

import me.jared.dungeoncrawler.api.plugin.DungeonCrawler;

import java.io.File;

public abstract class DataRegistry
{
    protected File folder;

    public DataRegistry(String folderName)
    {
        this.folder = new File(DungeonCrawler.getPlugin().getDataFolder() + "\\" + folderName);

        if (!folder.exists())
        {
            folder.mkdirs();
        }
    }
}
