package me.jared.dungeoncrawler.api.cell;

import me.jared.dungeoncrawler.api.decorations.IDecoration;
import me.jared.dungeoncrawler.api.structures.AABB;
import org.bukkit.util.Vector;

import java.util.List;

public interface ICell
{
    List<Vector> getDoorwayPoints();

    AABB getAABB();

    void setAABB(AABB aabb);

    Vector getCenter();

    double getArea();

    List<IDecoration> getDecorations();
}
