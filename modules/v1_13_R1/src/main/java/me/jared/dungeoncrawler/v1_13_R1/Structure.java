package me.jared.dungeoncrawler.v1_13_R1;

import com.google.common.collect.Maps;
import me.jared.dungeoncrawler.api.block.IMaterialAndData;
import me.jared.dungeoncrawler.api.structures.AbstractStructure;
import me.jared.dungeoncrawler.api.util.VectorUtil;
import me.jared.dungeoncrawler.v1_13_R1.block.MaterialAndData;
import net.minecraft.server.v1_13_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Directional;
import org.bukkit.craftbukkit.v1_13_R1.util.CraftMagicNumbers;
import org.bukkit.util.Vector;

import java.util.Map;

public class Structure extends AbstractStructure
{
    private DefinedStructure definedStructure;

    public Structure(DefinedStructure definedStructure)
    {
        this.definedStructure = definedStructure;

        NBTTagCompound tag = definedStructure.a(new NBTTagCompound());

        this.dimensions = new int[]{tag.getList("size", 3).h(0), tag.getList("size", 3).h(1), tag.getList("size", 3).h(2)};

        NBTTagList states = tag.getList("palette", 10);
        NBTTagList blocks = tag.getList("blocks", 10);

        for (int i = 0; i < blocks.size(); i++)
        {
            NBTTagCompound blockTag = blocks.getCompound(i);

            Vector position = new Vector(blockTag.getList("pos", 3).h(0), blockTag.getList("pos", 3).h(1), blockTag.getList("pos", 3).h(2));

            IBlockData data = GameProfileSerializer.d(states.getCompound(blockTag.getInt("state")));
            Block block = Block.REGISTRY.get(new MinecraftKey(states.getCompound(blockTag.getInt("state")).getString("Name")));

            Material material = CraftMagicNumbers.getMaterial(block);
            if (material == Material.LEGACY_SIGN_POST)
            {
                this.edgeCases.add(position);
            }
            else
            {
                blockMap.put(position, new MaterialAndData(CraftMagicNumbers.getMaterial(block), data.toString()));
            }
        }
    }

    public DefinedStructure getDefinedStructure()
    {
        return definedStructure;
    }

    @Override
    public void rotate(int angle)
    {
        Map<Vector, IMaterialAndData> blockMapCopy = Maps.newHashMap();

        for (Map.Entry<Vector, IMaterialAndData> blockEntry : blockMap.entrySet())
        {
            Vector vector = blockEntry.getKey();
            IMaterialAndData materialAndData = blockEntry.getValue();
            Material material = materialAndData.getMaterial();
            BlockData blockData = Bukkit.createBlockData(materialAndData.getBlockData());

            if (blockData instanceof Directional)
            {
                Directional directional = (Directional) blockData;
                Vector directionVector = VectorUtil.toVector(directional.getFacing());
                directionVector = VectorUtil.rotateVector(directionVector, angle);
                ((Directional) blockData).setFacing(VectorUtil.fromVector(directionVector));
            }

            Vector offset = VectorUtil.rotateVector(vector, angle);
            blockMapCopy.put(offset, new MaterialAndData(material, blockData.getAsString()));
        }

        this.blockMap = blockMapCopy;
    }
}
