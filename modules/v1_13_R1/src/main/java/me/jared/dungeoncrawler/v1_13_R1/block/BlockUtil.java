package me.jared.dungeoncrawler.v1_13_R1.block;

import com.google.common.collect.Lists;
import me.jared.dungeoncrawler.api.block.IBlockUtil;
import me.jared.dungeoncrawler.api.block.IMaterialAndData;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.data.BlockData;
import org.bukkit.material.MaterialData;

import java.util.List;

public class BlockUtil implements IBlockUtil
{
    @Override
    public boolean setBlockFast(Location location, MaterialData data)
    {
        return setBlockFast(location, data.getItemType(), data.getData());
    }

    @Override
    public boolean setBlockFast(Location location, Material material, byte legacyData)
    {
        return setBlockFast(location, new MaterialAndData(material, Bukkit.getUnsafe().fromLegacy(material, legacyData).getAsString()));
    }

    @Override
    public boolean setBlockFast(Location location, IMaterialAndData materialAndData)
    {
        return setBlockFast(location, materialAndData.getMaterial(), materialAndData.getBlockData());
    }

    public boolean setBlockFast(Location location, Material material, String blockData)
    {
        return setBlockFast(location.getWorld(), location.getBlockX(), location.getBlockY(), location.getBlockZ(), material, blockData);
    }

    public boolean setBlockFast(World world, int x, int y, int z, Material material, String blockData)
    {
        Block block = world.getBlockAt(x, y, z);
        BlockData data = Bukkit.getServer().createBlockData(blockData);
        BlockState state = block.getState();;
        state.setType(material);
        state.setBlockData(data);
        state.update(true);
        return true;
    }

    @Override
    public void refreshChunkLocations(List<Location> locations)
    {
        refreshChunks(getChunks(locations));
    }

    @Override
    public void refreshChunks(List<Chunk> chunks)
    {
        chunks.forEach(chunk -> chunk.getWorld().refreshChunk(chunk.getX(), chunk.getZ()));
    }

    @Override
    public List<Chunk> getChunks(List<Location> locations)
    {
        List<Chunk> chunks = Lists.newArrayList();

        for (Location location : locations)
        {
            Chunk chunk = location.getChunk();

            if (!chunks.contains(chunk))
                chunks.add(chunk);
        }

        return chunks;
    }
}
