package me.jared.dungeoncrawler.v1_13_R1.block;

import me.jared.dungeoncrawler.api.block.AbstractMaterialAndData;
import org.bukkit.Material;

public class MaterialAndData extends AbstractMaterialAndData
{
    private String blockData;

    public MaterialAndData(Material material, String data)
    {
        super(material);
        this.blockData = data;
    }

    @Override
    public String getBlockData()
    {
        return blockData;
    }

    @Override
    public byte getBlockDataLegacy()
    {
        return -1;
    }
}
