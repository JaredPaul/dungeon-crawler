package me.jared.dungeoncrawler.states;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import me.jared.dungeoncrawler.*;
import me.jared.dungeoncrawler.api.plugin.DungeonCrawler;
import me.jared.dungeoncrawler.cell.Cell;
import me.jared.dungeoncrawler.cell.CellType;
import me.jared.dungeoncrawler.cell.CellUtil;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Wool;
import org.bukkit.util.Vector;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

public class IntersectionState implements State
{
    @Override
    public int order()
    {
        return 8;
    }

    @Override
    public void run(GenerationContext generationContext)
    {
        DungeonCrawler.LOG.info("Building Rooms...");

        List<Cell> cellsMaster = generationContext.cellsMaster;
        cellsMaster.addAll(generationContext.intersectingCells);

        List<Vector> rooms = Lists.newArrayList();
        List<Vector> none = Lists.newArrayList();

        Iterator<Cell> cellIterator = generationContext.intersectingCells.iterator();
        while (cellIterator.hasNext())
        {
            Cell cell = cellIterator.next();
            List<Vector> cellVectors = getCellVectors(cell);

            if (cell.type == CellType.ROOM)
            {
                rooms.addAll(getCellVectors(cell));
            }
            else if (cell.type == CellType.FILL)
            {
                cellIterator.remove();
            }
            /*
            else if (cell.type == CellType.NONE)
            {
                none.addAll(getCellVectors(cell));
            }
            */

            for (Vector cellVector : cellVectors)
            {
                Random random = ThreadLocalRandom.current();
                int mainChance = random.nextInt(100);

                MaterialData data;
                if (mainChance < 50)
                {
                    int chance = random.nextInt(100);

                    if (chance < 50)
                    {
                        DungeonCrawler.LOG.debug("awf");
                        data = new MaterialData(Material.SMOOTH_BRICK, (byte) 1);
                    }
                    else
                    {
                        data = new MaterialData(Material.SMOOTH_BRICK, (byte) 2);
                    }
                }
                else
                {
                    data = new MaterialData(Material.SMOOTH_BRICK);
                }

                generationContext.drawImmediately(Lists.newArrayList(cellVector), data);
            }
        }

        //generationContext.drawInQueue(new Tuple<>(hallways, new MaterialData(Material.NETHER_BRICK)));
        //generationContext.drawInQueue(none, new MaterialData(Material.STONE));

        generationContext.setState(new DoorwayState());
        generationContext.runState();
    }

    private Set<Cell> getIntersectingCells(Vector p1, Vector p2, List<Cell> cells, int extraWidth)
    {
        Set<Cell> store = Sets.newHashSet();
        Vector vector = new Vector();
        int x1, z1, x2, z2;
        if (p1.getBlockX() == p2.getBlockX())
        {
            z1 = Math.min(p1.getBlockZ(), p2.getBlockZ()) - extraWidth;
            z2 = Math.max(p1.getBlockZ(), p2.getBlockZ()) + extraWidth;

            for (int x = p1.getBlockX() - extraWidth; x <= p1.getBlockX() + extraWidth; x++)
            {
                for (int z = z1; z <= z2; z++)
                {
                    vector.setX(x);
                    vector.setZ(z);

                    Cell cell = CellUtil.getCell(vector, cells);
                    if (cell != null)
                    {
                        store.add(cell);
                    }
                }
            }
        }
        else if (p1.getBlockZ() == p2.getBlockZ())
        {
            x1 = Math.min(p1.getBlockX(), p2.getBlockX()) - extraWidth;
            x2 = Math.max(p1.getBlockX(), p2.getBlockX()) + extraWidth;
            for (int y = p1.getBlockZ() - extraWidth; y <= p1.getBlockZ() + extraWidth; y++)
            {
                for (int x = x1; x <= x2; x++)
                {
                    vector.setX(x);
                    vector.setZ(y);

                    Cell cell = CellUtil.getCell(vector, cells);
                    if (cell != null)
                    {
                        store.add(cell);
                    }
                }
            }
        }

        return store;
    }

    private List<Vector> getCellVectors(Cell cell)
    {
        double x1 = cell.getAABB().getMaxX();
        double z1 = cell.getAABB().getMinZ();
        double x2 = cell.getAABB().getMinX();
        double z2 = cell.getAABB().getMaxZ();

        List<Vector> vectors = Lists.newArrayList();

        for (double x = x2; x <= x1; x++)
        {
            for (double z = z1; z <= z2; z++)
            {
                Vector vector = new Vector(x, 0, z);
                vectors.add(vector);

                double height = cell.getAABB().getMaxY();
                while (height > 0)
                {
                    Vector additionVector = new Vector(0, height, 0);
                    Vector ceilingVector = vector.clone().add(additionVector);

                    if (cell.type == CellType.ROOM)
                    {
                        if (x == cell.getAABB().getMinX() ||
                                z == cell.getAABB().getMinZ() ||
                                z == cell.getAABB().getMaxZ() ||
                                x == cell.getAABB().getMaxX())
                        {
                            vectors.add(ceilingVector);
                        }
                    }

                    height--;
                }


            }
        }

        return vectors;
    }
}
