package me.jared.dungeoncrawler.decorations.states;

import me.jared.dungeoncrawler.GenerationContext;
import me.jared.dungeoncrawler.decorations.DecorationLoader;

public interface IDecorationLoadingState
{
    void run(GenerationContext context, DecorationLoader decorationLoader);
}
