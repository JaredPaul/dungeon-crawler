package me.jared.dungeoncrawler;

import me.jared.dungeoncrawler.api.structures.AABB;
import me.jared.dungeoncrawler.cell.Cell;

import java.util.List;

public class Map
{
    //opposites for get()
    private double xmin = Integer.MAX_VALUE;
    private double xmax = Integer.MIN_VALUE;
    private double zmin = Integer.MAX_VALUE;
    private double zmax = Integer.MIN_VALUE;

    private Map()
    {
    }

    public double getLeft()
    {
        return xmin;
    }

    public double getRight()
    {
        return xmax;
    }

    public double getTop()
    {
        return zmax;
    }

    public double getBottom()
    {
        return zmin;
    }

    public double getWidth()
    {
        return xmax - xmin;
    }

    public double getLength()
    {
        return zmax - zmin;
    }

    public static Map get(List<Cell> cells)
    {
        Map cb = new Map();
        double i;
        for (Cell cell : cells)
        {
            AABB bounds = cell.getAABB();
            i = bounds.getMinX();
            if (i < cb.xmin)
            {
                cb.xmin = i;
            }
            i = bounds.getMaxX();
            if (i > cb.xmax)
            {
                cb.xmax = i;
            }
            i = bounds.getMinZ();
            if (i < cb.zmin)
            {
                cb.zmin = i;
            }
            i = bounds.getMaxZ();
            if (i > cb.zmax)
            {
                cb.zmax = i;
            }
        }

        return cb;
    }

    @Override
    public String toString()
    {
        return "CellBounds [xmin=" + xmin + ", xmax=" + xmax + ", ymin=" + zmin
                + ", ymax=" + zmax + "]";
    }
}
