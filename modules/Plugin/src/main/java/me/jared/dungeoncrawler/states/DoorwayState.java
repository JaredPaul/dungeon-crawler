package me.jared.dungeoncrawler.states;

import com.google.common.collect.Lists;
import me.jared.dungeoncrawler.cell.Cell;
import me.jared.dungeoncrawler.GenerationContext;
import me.jared.dungeoncrawler.api.plugin.DungeonCrawler;
import me.jared.dungeoncrawler.api.util.VectorUtil;
import org.bukkit.Material;
import org.bukkit.material.MaterialData;
import org.bukkit.util.Vector;

import java.util.List;

public class DoorwayState implements State
{
    @Override
    public int order()
    {
        return 10;
    }

    @Override
    public void run(GenerationContext generationContext)
    {
        DungeonCrawler.LOG.info("Clearing Doorways...");

        List<Vector> vectors = Lists.newArrayList();

        for (Cell cell : generationContext.cellsMaster)
        {
            for (Vector doorway : cell.doorways)
            {
                vectors.addAll(VectorUtil.getDoorwayVectors(doorway));
            }
        }

        generationContext.drawInQueue(vectors, new MaterialData(Material.AIR));

        generationContext.setState(new ChestState());
        generationContext.runState();
    }
}
