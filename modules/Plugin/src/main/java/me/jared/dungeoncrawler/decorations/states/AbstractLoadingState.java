package me.jared.dungeoncrawler.decorations.states;

import me.jared.dungeoncrawler.cell.Cell;
import me.jared.dungeoncrawler.GenerationContext;
import me.jared.dungeoncrawler.api.decorations.IDecoration;
import me.jared.dungeoncrawler.api.plugin.DungeonCrawler;
import me.jared.dungeoncrawler.decorations.DecorationLoader;
import org.bukkit.Bukkit;
import org.bukkit.util.Vector;

import java.util.List;

public abstract class AbstractLoadingState implements IDecorationLoadingState
{
    private IDecoration.Size size;
    private int amount;

    public AbstractLoadingState(IDecoration.Size size, int amount)
    {
        this.size = size;
        this.amount = amount;
    }

    @Override
    public void run(GenerationContext context, DecorationLoader decorationLoader)
    {
        String capitalizedSize = size.name().substring(0, 1).toUpperCase() + size.name().substring(1);
        DungeonCrawler.LOG.info("   Loading " + capitalizedSize + " Decorations...");

        List<Cell> cells = context.cellsMaster;

        for (Cell cell : cells)
        {
            for (int i = 0; i < amount; i++)
            {
                IDecoration decoration = DungeonCrawler.getDecorationRegistry().getRandomDecoration(cell, size);

                if (decoration != null)
                {
                    final IDecoration decorationFinal = decoration.clone();

                    Vector suitablePosition = decorationLoader.getSuitablePosition(decorationFinal, cell);
                    if (suitablePosition != null)
                    {
                        cell.getDecorations().add(decorationFinal);

                        Bukkit.getScheduler().runTask(DungeonCrawler.getPlugin(), () ->
                                decorationFinal.place(context.base));
                    }
                }
            }
        }
    }
}
