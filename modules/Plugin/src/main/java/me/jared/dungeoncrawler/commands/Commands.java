package me.jared.dungeoncrawler.commands;

import me.jared.dungeoncrawler.GenerationContext;
import me.jared.dungeoncrawler.commands.Command;
import me.jared.dungeoncrawler.commands.CommandClass;
import me.jared.dungeoncrawler.commands.CommandContext;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Commands implements CommandClass
{
    @Command(
            aliases = {"dungeon"},
            usage = "/dungeon create",
            description = "creates a dungeon",
            initializer = "create",
            minArgs = 0,
            maxArgs = 0
    )
    public void create(CommandContext args, CommandSender sender)
    {
        Player player = (Player) sender;

        new GenerationContext(player.getLocation());
    }
}
