package me.jared.dungeoncrawler.states;

import me.jared.dungeoncrawler.*;
import me.jared.dungeoncrawler.cell.RoomConnection;
import me.jared.dungeoncrawler.delaunay.DT_Point;
import me.jared.dungeoncrawler.delaunay.EdgeList;
import me.jared.dungeoncrawler.util.XORShiftRandom;
import org.bukkit.util.Vector;

import java.util.List;

public class ConnectState implements State
{
    @Override
    public int order()
    {
        return 7;
    }

    @Override
    public void run(GenerationContext generationContext)
    {
        List<EdgeList.Edge> minTree = generationContext.minTree;
        List<RoomConnection> roomConnections = generationContext.roomConnections;

        XORShiftRandom random = generationContext.random;

        Vector baseVector = generationContext.baseVector;

        int size = minTree.size();
        while (size > 0)
        {
            EdgeList.Edge edge = minTree.get(size - 1);
            Vector start = getVectorFromDTPoint(edge.getP1());
            Vector end = getVectorFromDTPoint(edge.getP2());

            if (start.getBlockX() == end.getBlockX() || start.getBlockZ() == end.getBlockZ())
            {
                roomConnections.add(RoomConnection.get(start, end));
            }
            else
            {
                if (random.nextBoolean())
                {
                    baseVector.setX(start.getBlockX());
                    baseVector.setZ(end.getBlockZ());
                }
                else
                {
                    baseVector.setX(end.getBlockX());
                    baseVector.setZ(start.getBlockZ());
                }

                roomConnections.add(RoomConnection.get(start, baseVector, end));
            }

            size--;
        }

        //generationContext.setState(new HallwayState());
        //generationContext.runState();
    }

    private Vector getVectorFromDTPoint(DT_Point p)
    {
        return new Vector((int) p.x(), 0, (int) p.y());
    }
}
