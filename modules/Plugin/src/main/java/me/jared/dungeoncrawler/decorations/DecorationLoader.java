package me.jared.dungeoncrawler.decorations;

import com.google.common.collect.Lists;
import me.jared.dungeoncrawler.GenerationContext;
import me.jared.dungeoncrawler.api.cell.ICell;
import me.jared.dungeoncrawler.api.decorations.IDecoration;
import me.jared.dungeoncrawler.api.structures.AABB;
import me.jared.dungeoncrawler.api.util.VectorUtil;
import me.jared.dungeoncrawler.decorations.states.BigLoadingState;
import me.jared.dungeoncrawler.decorations.states.IDecorationLoadingState;
import org.bukkit.util.Vector;

import java.lang.reflect.Constructor;
import java.util.Collections;
import java.util.List;

public class DecorationLoader
{
    private GenerationContext generationContext;

    private IDecorationLoadingState state;

    public DecorationLoader(GenerationContext generationContext)
    {
        this.generationContext = generationContext;
        setState(new BigLoadingState());
    }

    public void setState(IDecorationLoadingState state)
    {
        this.state = state;
    }

    public void run()
    {
        state.run(generationContext, this);
    }

    public Vector getSuitablePosition(IDecoration decoration, ICell cell)
    {
        List<Vector> basePoints = cell.getAABB().getAllPoints();
        AABB cellAABB = cell.getAABB().clone();
        cellAABB.grow(-1, -1, -1);

        List<Vector> allPoints = cellAABB.getAllPoints();
        basePoints.removeAll(allPoints);

        List<Vector> doorwayPoints = Lists.newArrayList();
        for (Vector doorway : cell.getDoorwayPoints())
        {
            doorwayPoints.addAll(VectorUtil.getDoorwayVectors(doorway));
        }
        basePoints.addAll(doorwayPoints);

        //idk a better way of doing this
        //decorations only get added to the cell when it's placed
        //thus meaning decoration.getPosition() will never be null
        List<IDecoration> decorations = cell.getDecorations();
        for (IDecoration cellDecoration : decorations)
        {
            List<Vector> points = cellDecoration.getStructure().getPoints(cellDecoration.getPosition());
            basePoints.addAll(points);
        }

        List<Vector> walls = cellAABB.getFacePoints(AABB.Face.WALLS);
        List<Vector> corners = cellAABB.getFacePoints(AABB.Face.CORNERS);
        List<Vector> top = cellAABB.getFacePoints(AABB.Face.TOP);
        List<Vector> bottom = cellAABB.getFacePoints(AABB.Face.BOTTOM);

        Collections.shuffle(allPoints);
        for (Vector point : allPoints)
        {
            Vector returnPoint = null;

            List<Rotation> rotations = Lists.newArrayList(Rotation.values());
            Collections.shuffle(rotations);
            for (Rotation rotation : rotations)
            {
                int angle = rotation.angle;

                List<Vector> rotatedEdgeCases = decoration.getStructure().getRotatedEdgeCases(point, angle);

                if (!rotatedEdgeCases.isEmpty())
                {
                    if (!isNearWalls(walls, rotatedEdgeCases))
                        continue;
                }

                AABB rotated = decoration.getStructure().getTestAABB(point, angle);

                if (rotated.intersects(basePoints))
                    continue;

                switch (decoration.getType())
                {
                    case WALL:
                        if (rotated.intersects(walls))
                            returnPoint = point;
                        break;
                    case CORNER:
                        if (rotated.intersects(corners))
                            returnPoint = point;
                        break;
                    case CEILING_CORNER:
                        if (rotated.intersects(top) && rotated.intersects(corners))
                            returnPoint = point;
                        break;
                    case CEILING_WALL:
                        if (rotated.intersects(top) && rotated.intersects(walls))
                            returnPoint = point;
                        break;
                    case FLOOR:
                        if (rotated.intersects(bottom))
                            returnPoint = point;
                        break;
                    case FLOOR_CORNER:
                        if (rotated.intersects(bottom) && rotated.intersects(corners))
                            returnPoint = point;
                        break;
                    case FLOOR_WALL:
                        if (rotated.intersects(bottom) && rotated.intersects(walls))
                            returnPoint = point;
                        break;
                }

                if (returnPoint != null)
                {
                    decoration.setRotation(angle);
                    decoration.setPosition(returnPoint);

                    return returnPoint;
                }
            }
        }

        return null;
    }

    private boolean isNearWalls(List<Vector> walls, List<Vector> edges)
    {
        List<Boolean> booleans = Lists.newArrayList();

        for (Vector edge : edges)
        {
            if (walls.contains(edge))
            {
                booleans.add(true);
            }
            else
            {
                booleans.add(false);
            }
        }

        return isAllTrue(booleans);
    }

    private boolean isAllTrue(List<Boolean> booleans)
    {
        for (Boolean bool : booleans)
        {
            if (bool != true)
            {
                return false;
            }
        }

        return true;
    }

    private void shuffleLists(List... lists)
    {
        for (List list : lists)
        {
            Collections.shuffle(list);
        }
    }

    /*
    public boolean isNearCorner(Bounds bounds, List<Vector> testPoints)
    {
        for (Direction direction : Direction.values())
        {
            for (Vector testPoint : testPoints)
            {
                Vector testOffset = testPoint.clone().add(direction.toVector());

                List<Vector> corners = bounds.getCorners();
                for (Vector vector : corners)
                {
                    Vector offset = vector.clone().setY(testOffset.getBlockY());

                    if (testOffset.equals(offset))
                        return true;
                }
            }
        }

        return false;
    }
    */

    public boolean isNearHeight(List<Vector> testPoints, int height)
    {
        for (Vector testPoint : testPoints)
        {
            if (testPoint.getBlockY() == height)
                return true;
        }

        return false;
    }

    private List<Vector> getPoints(ICell cell, IDecoration decoration)
    {


        switch (decoration.getType())
        {
            case WALL:

        }

        return null;
    }

    enum Rotation
    {
        NONE(0),
        NINETY(90),
        ONE_EIGHTY(180),
        TWO_SEVENTY(270),;

        int angle;

        Rotation(int angle)
        {
            this.angle = angle;
        }
    }

    interface C
    {

    }

    class AbstractC implements C
    {

    }

    class AFollowC extends AbstractC
    {
        public AFollowC(String a)
        {
        }
    }

    class BFollowC extends AbstractC
    {
        public BFollowC(String a, String b)
        {
        }
    }

    AbstractC test(String cType, String[] args) throws Exception
    {
        Class<?> clazz = Class.forName(cType + "FollowC");

        if (clazz.isAssignableFrom(AbstractC.class))
        {
            Constructor<?>[] constructors = clazz.getConstructors();

            for (Constructor<?> constructor : constructors)
            {
                if (constructor.getParameterCount() == args.length)
                {
                    return (AbstractC) constructor.newInstance(args);
                }
            }
        }

        return null;
    }
}
