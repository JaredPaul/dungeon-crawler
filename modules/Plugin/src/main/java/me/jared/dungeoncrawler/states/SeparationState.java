package me.jared.dungeoncrawler.states;

import com.google.common.collect.Lists;
import me.jared.dungeoncrawler.*;
import me.jared.dungeoncrawler.api.plugin.DungeonCrawler;
import me.jared.dungeoncrawler.api.structures.AABB;
import me.jared.dungeoncrawler.cell.Cell;
import me.jared.dungeoncrawler.cell.CellType;
import me.jared.dungeoncrawler.util.XORShiftRandom;
import org.bukkit.DyeColor;
import org.bukkit.util.Vector;

import java.util.Collections;
import java.util.List;
import java.util.Random;

public class SeparationState implements State
{
    @Override
    public int order()
    {
        return 2;
    }

    @Override
    public void run(GenerationContext generationContext)
    {
        DungeonCrawler.LOG.info("Separating Cells...");

        List<Cell> cells = generationContext.cells;
        XORShiftRandom random = generationContext.random;

        while (isAnyOverlap(generationContext.cells))
        {
            moveCells(random, cells);
        }

        generationContext.setState(new FillState());
        generationContext.runState();
    }

    public DyeColor getRandomColour()
    {
        List<DyeColor> materials = Lists.newArrayList(DyeColor.values());
        Collections.shuffle(materials);

        return materials.get(0);
    }

    public static void moveCells(Random rand, List<Cell> cells)
    {

        Cell cell;
        Vector v = new Vector();
        for (int i = 0; i < cells.size(); i++)
        {
            cell = cells.get(i);
            v.setX(0);
            v.setY(0);
            v.setZ(0);
            for (int j = 0; j < cells.size(); j++)
            {
                if (i == j)
                    continue;
                if (isOverlapping(cell, cells.get(j)))
                {
                    moveCell(rand, cell, cells.get(j), v);
                }
            }

            cell.getAABB().move(v.getBlockX(), v.getBlockY(), v.getBlockZ());
        }

    }

    private static void moveCell(Random rand, Cell c1, Cell c2, Vector v)
    {
        int dx = clamp((int) (c1.getAABB().getBottomCenterX() - c2.getAABB().getBottomCenterX()), -1, 1);
        int dy = clamp((int) (c1.getAABB().getBottomCenterZ() - c2.getAABB().getBottomCenterZ()), -1, 1);
        if (dx == 0 && dy == 0)
        {
            switch (rand.nextInt(4))
            {
                case 0:
                    dx = 1;
                    break;
                case 1:
                    dx = -1;
                    break;
                case 2:
                    dy = 1;
                    break;
                case 3:
                    dy = -1;
                    break;
            }
        }
        v.setX(v.getBlockX() + dx);
        v.setZ(v.getBlockZ() + dy);
    }

    private static int clamp(int input, int min, int max)
    {
        if (input < min)
            return min;
        else if (input > max)
            return max;
        else
            return input;
    }

    private static boolean isAnyOverlap(List<Cell> cells)
    {
        Cell cell;
        for (int i = 0; i < cells.size(); i++)
        {
            cell = cells.get(i);
            for (int j = 0; j < cells.size(); j++)
            {
                if (i == j)
                    continue;
                if (isOverlapping(cell, cells.get(j)))
                {
                    cell.type = CellType.OVERLAP;
                    cells.get(j).type = CellType.OVERLAP;
                    return true;
                }
                else
                {
                    cell.type = CellType.NONE;
                    cells.get(j).type = CellType.NONE;
                }
            }
        }
        return false;
    }

    private static boolean isOverlapping(Cell c1, Cell c2)
    {
        AABB clone = c1.getAABB().clone();
        clone.grow(1, 0, 1);

        AABB clone2 = c2.getAABB().clone();
        clone2.grow(1, 0, 1);

        return clone.intersects(clone2);
    }
}
