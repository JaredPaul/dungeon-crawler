package me.jared.dungeoncrawler.states;

import com.google.common.collect.Lists;
import me.jared.dungeoncrawler.cell.Cell;
import me.jared.dungeoncrawler.cell.CellUtil;
import me.jared.dungeoncrawler.GenerationContext;
import me.jared.dungeoncrawler.api.plugin.DungeonCrawler;
import me.jared.dungeoncrawler.api.util.Direction;
import me.jared.dungeoncrawler.api.util.VectorUtil;
import org.bukkit.block.BlockFace;
import org.bukkit.material.Chest;
import org.bukkit.material.MaterialData;
import org.bukkit.util.Vector;

public class ChestState implements State
{
    @Override
    public int order()
    {
        return 11;
    }

    @Override
    public void run(GenerationContext generationContext)
    {
        DungeonCrawler.LOG.info("Spawning Supply Chests...");

        for (Cell cell : generationContext.cellsMaster)
        {
            int chance = 5;
            int random = generationContext.random.nextInt(100);

            if (random < chance)
            {
                int mainCount = 0;
                for (Vector doorway : cell.doorways)
                {
                    for (Direction direction : Direction.getMainDirections())
                    {
                        Vector offset = doorway.clone().add(direction.toVector());

                        if (generationContext.hallways.contains(offset))
                            doorway = offset;
                    }

                    if (mainCount >= 1)
                        continue;

                    //dependant on hallway width
                    double xmin = -3;
                    double xmax = 3;
                    double zmin = -3;
                    double zmax = 3;

                    boolean rand = generationContext.random.nextBoolean();
                    Vector toAddCase1;
                    Vector toAddCase2;

                    if (rand)
                    {
                        xmin = 0;
                        xmax = 0;
                        toAddCase1 = new Vector(0, 0, -1);
                        toAddCase2 = new Vector(0, 0, 1);
                    }
                    else
                    {
                        zmin = 0;
                        zmax = 0;
                        toAddCase1 = new Vector(-1, 0, 0);
                        toAddCase2 = new Vector(1, 0, 0);
                    }

                    DungeonCrawler.LOG.info("1");

                    int count = 0;
                    for (Vector corner : VectorUtil.getCorners(xmin, xmax, zmin, zmax))
                    {
                        if (count >= 1)
                            continue;

                        DungeonCrawler.LOG.info("2");

                        int x = corner.getBlockX();
                        int z = corner.getBlockZ();

                        corner = corner.clone().add(doorway);

                        if (CellUtil.isPointInCells(corner, null, generationContext.cellsMaster))
                        {
                            continue;
                        }
                        else if (generationContext.hallways.contains(corner))
                        {
                            DungeonCrawler.LOG.info("4");
                        }

                        DungeonCrawler.LOG.info("3");

                        Vector up = new Vector(0, 1, 0);
                        Vector vector;

                        if (x == -3 || z == -3)
                        {
                            vector = corner.clone().add(toAddCase2).add(up);
                        }
                        else
                        {
                            vector = corner.clone().add(toAddCase1).add(up);
                        }

                        BlockFace direction = getDirection(vector, generationContext);

                        if (direction != null)
                        {
                            DungeonCrawler.LOG.warning(direction);

                            draw(generationContext, vector, new Chest(direction));
                            mainCount++;
                            count++;
                        }
                    }
                }
            }
        }

        generationContext.setState(new DecorationState());
        generationContext.runState();
    }

    private void draw(GenerationContext generationContext, Vector vector, MaterialData data)
    {
        generationContext.drawImmediately(Lists.newArrayList(vector), data);
    }

    private BlockFace getDirection(Vector vector, GenerationContext context)
    {
        for (Direction direction : Direction.getMainDirections())
        {
            Vector testPoint = vector.clone().add(direction.toVector());

            if (CellUtil.getCell(testPoint, context.cellsMaster) == null)
                continue;

            return direction.toBlockFace().getOppositeFace();
        }

        return null;
    }
}
