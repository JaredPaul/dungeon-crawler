package me.jared.dungeoncrawler.states;

import me.jared.dungeoncrawler.GenerationContext;

public interface State
{
    int order();

    void run(GenerationContext generationContext);
}
