package me.jared.dungeoncrawler.cell;

import com.google.common.collect.Lists;
import org.bukkit.util.Vector;

import java.util.List;

public class RoomConnection
{
    private List<Vector> points = Lists.newArrayList();

    public List<Vector> getPoints()
    {
        return points;
    }

    public static RoomConnection get(Vector start, Vector end)
    {
        RoomConnection con = new RoomConnection();
        con.points.add(new Vector(start.getBlockX(), 0, start.getBlockZ()));
        con.points.add(new Vector(end.getBlockX(), 0, end.getBlockZ()));

        return con;
    }

    public static RoomConnection get(Vector start, Vector elbow, Vector end)
    {
        RoomConnection con = new RoomConnection();
        con.points.add(new Vector(start.getBlockX(), 0, start.getBlockZ()));
        con.points.add(new Vector(elbow.getBlockX(), 0, elbow.getBlockZ()));
        con.points.add(new Vector(end.getBlockX(), 0, end.getBlockZ()));

        return con;
    }
}
