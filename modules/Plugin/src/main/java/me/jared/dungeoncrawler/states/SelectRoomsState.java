package me.jared.dungeoncrawler.states;

import com.google.common.collect.Lists;
import me.jared.dungeoncrawler.*;
import me.jared.dungeoncrawler.api.plugin.DungeonCrawler;
import me.jared.dungeoncrawler.cell.Cell;
import me.jared.dungeoncrawler.cell.CellType;
import me.jared.dungeoncrawler.cell.sorter.Strategies;

import java.util.List;

public class SelectRoomsState implements State
{
    @Override
    public int order()
    {
        return 4;
    }

    @Override
    public void run(GenerationContext generationContext)
    {
        DungeonCrawler.LOG.info("Selecting Rooms...");

        List<Cell> cells = generationContext.cells;
        List<Cell> rooms = generationContext.rooms;
        int roomAreaThreshold = generationContext.roomAreaThreshold;
        int minRooms = generationContext.minRooms;

        for (Cell cell : cells)
        {
            if (cell.getArea() > roomAreaThreshold)
            {
                rooms.add(cell);
                cell.type = CellType.ROOM;
            }
        }

        if (rooms.size() < minRooms)
        {
            List<Cell> orderedCells = Lists.newArrayList(cells);

            if (!rooms.isEmpty())
                orderedCells.removeAll(rooms);

            orderedCells.sort(Strategies.getCellAreaComparator());
            int augment = 0;

            Cell cell;
            while (rooms.size() < minRooms)
            {
                cell = orderedCells.remove(0);
                cell.type = CellType.ROOM;
                rooms.add(cell);
                augment++;
            }
        }

        generationContext.setState(new TriangulateState());
        generationContext.runState();
    }
}
