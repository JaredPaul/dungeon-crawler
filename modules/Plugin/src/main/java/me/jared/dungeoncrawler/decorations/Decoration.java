package me.jared.dungeoncrawler.decorations;

import me.jared.dungeoncrawler.api.decorations.IDecoration;
import me.jared.dungeoncrawler.api.structures.IStructure;
import org.bukkit.Location;
import org.bukkit.util.Vector;

public class Decoration implements IDecoration
{
    private int rotation;
    private Vector position;
    private Type type;
    private Size size;
    private IStructure structure;

    public Decoration(IStructure structure, Size size, Type type)
    {
        this.structure = structure;
        this.size = size;
        this.type = type;
    }

    @Override
    public String getName()
    {
        return structure.getName();
    }

    @Override
    public int getRotation()
    {
        return rotation;
    }

    @Override
    public void setRotation(int rotation)
    {
        this.rotation = rotation;
    }

    @Override
    public Vector getPosition()
    {
        return position;
    }

    @Override
    public void setPosition(Vector position)
    {
        this.position = position;
    }

    @Override
    public IStructure getStructure()
    {
        return structure;
    }

    @Override
    public Size getSize()
    {
        return size;
    }

    @Override
    public Type getType()
    {
        return type;
    }

    @Override
    public void place(Location base)
    {
        structure.rotate(rotation);
        structure.place(base.clone().add(position));
    }

    @Override
    public Decoration clone()
    {
        try
        {
            Decoration decoration = (Decoration) super.clone();
            decoration.structure = decoration.structure.clone();
            return decoration;
        } catch (CloneNotSupportedException e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
