package me.jared.dungeoncrawler.states;

import com.google.common.collect.Lists;
import me.jared.dungeoncrawler.cell.Cell;
import me.jared.dungeoncrawler.GenerationContext;
import me.jared.dungeoncrawler.util.XORShiftRandom;
import me.jared.dungeoncrawler.api.plugin.DungeonCrawler;
import me.jared.dungeoncrawler.api.structures.AABB;
import org.bukkit.DyeColor;
import org.bukkit.util.Vector;

import java.util.Collections;
import java.util.List;
import java.util.Random;

public class GenerateCellsState implements State
{
    @Override
    public int order()
    {
        return 1;
    }

    @Override
    public void run(GenerationContext generationContext)
    {
        DungeonCrawler.LOG.info("Generating Cells...");

        generationContext.cellCount = 10;

        int cellCount = generationContext.cellCount;
        XORShiftRandom random = generationContext.random;
        Vector cellSize = generationContext.cellSize;
        Vector areaCenter = generationContext.areaCenter;
        int radius = generationContext.radius;
        List<Cell> cells = generationContext.cells;

        for (int i = 0; i < cellCount; i++)
        {
            Cell cell = generateCell(random, cellSize, areaCenter, radius, cells);
        }

        generationContext.setState(new SeparationState());
        generationContext.runState();
    }

    public DyeColor getRandomColour()
    {
        List<DyeColor> materials = Lists.newArrayList(DyeColor.values());
        Collections.shuffle(materials);

        return materials.get(0);
    }



    private Cell generateCell(Random rand, Vector size, Vector center, int radius, List<Cell> cells)
    {
        int rSqr = radius * radius;

        int minx = center.getBlockX() - radius;
        int maxx = center.getBlockX() + radius;
        int minz = center.getBlockZ() - radius;
        int maxz = center.getBlockZ() + radius;

        int rangex = maxx - minx;
        int rangez = maxz - minz;

        int cx, cy;

        Cell cell = new Cell();

        int timeout = 1000;

        Vector testCenter = new Vector();

        while (testCenter.distanceSquared(center) > rSqr)
        {
            if (timeout <= 0)
                break;

            testCenter = new Vector(rand.nextInt(rangex) + minx, 0, rand.nextInt(rangez) + minz);

            timeout--;
        }

        float cRatio = (float) (1.0f - nextClampedGaussian(rand) * 0.33f);
        float cSize = (float) (nextClampedGaussian(rand) * (size.getBlockZ() - size.getBlockX() + 1) + size.getBlockX());

        if (rand.nextBoolean())
        {
            cx = (int) cSize;
            cy = (int) (cSize * cRatio);
        }
        else
        {
            cx = (int) (cSize * cRatio);
            cy = (int) cSize;
        }

        int posx = testCenter.getBlockX() + (cx / 2);
        int posz = testCenter.getBlockZ() + (cy / 2);

        AABB aabb = new AABB(posx, testCenter.getBlockY(), posz, posx + cx + 20, 10, posz + cy + 20);

        cell.setAABB(aabb);

        cells.add(cell);

        return cell;
    }

    public double nextClampedGaussian(Random rand)
    {
        double g = rand.nextGaussian();
        g = g < 0 ? -g : g;
        g /= MAX_GAUSS;
        return g > 1.0 ? 1.0 : g;
    }

    private static final double MAX_GAUSS = 6.418382187553036;
}
