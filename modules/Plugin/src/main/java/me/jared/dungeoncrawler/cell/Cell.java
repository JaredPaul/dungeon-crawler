package me.jared.dungeoncrawler.cell;

import com.google.common.collect.Lists;
import me.jared.dungeoncrawler.api.cell.ICell;
import me.jared.dungeoncrawler.api.decorations.IDecoration;
import me.jared.dungeoncrawler.api.structures.AABB;
import org.bukkit.util.Vector;

import java.util.List;

public class Cell implements ICell
{
    private Vector position;
    private AABB aabb;

    public List<Vector> doorways = Lists.newArrayList();
    public CellType type;

    public List<IDecoration> decorations = Lists.newArrayList();

    public Cell()
    {
        this.type = CellType.NONE;
    }

    @Override
    public List<Vector> getDoorwayPoints()
    {
        return doorways;
    }

    @Override
    public AABB getAABB()
    {
        return aabb;
    }

    @Override
    public void setAABB(AABB aabb)
    {
        this.aabb = aabb;
    }

    @Override
    public Vector getCenter()
    {
        int x = (int) (aabb.minX + (aabb.getWidth() / 2));
        int z = (int) (aabb.minZ + (aabb.getDepth() / 2));
        return new Vector(x, 0, z);
    }

    @Override
    public double getArea()
    {
        return aabb.getDepth() * aabb.getWidth();
    }

    @Override
    public List<IDecoration> getDecorations()
    {
        return decorations;
    }
}
