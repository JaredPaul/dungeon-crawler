package me.jared.dungeoncrawler;

import me.jared.dungeoncrawler.api.block.IBlockUtil;
import me.jared.dungeoncrawler.api.plugin.DungeonCrawler;
import me.jared.dungeoncrawler.api.plugin.IDungeonCrawlerMain;
import me.jared.dungeoncrawler.api.registry.decorations.IDecorationRegistry;
import me.jared.dungeoncrawler.api.structures.IStructureUtil;
import me.jared.dungeoncrawler.commands.CommandManager;
import me.jared.dungeoncrawler.commands.Commands;
import me.jared.dungeoncrawler.commands.SimpleInjector;
import me.jared.dungeoncrawler.decorations.DecorationRegistry;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import java.io.IOException;

public class DungeonCrawlerMain extends JavaPlugin implements Listener, IDungeonCrawlerMain
{
    private static final CommandManager COMMANDS = new CommandManager();
    private IDecorationRegistry decorationRegistry;
    private IStructureUtil structureUtil;
    private IBlockUtil blockUtil;

    public void onEnable()
    {
        DungeonCrawler.setPlugin(this);

        String packageName = this.getServer().getClass().getPackage().getName();
        String version = packageName.substring(packageName.lastIndexOf(".") + 1);

        try
        {
            DungeonCrawler.LOG.warning(version);

            String name = "me.jared.dungeoncrawler." + version + ".StructureUtil";

            final Class<?> decorationClazz = Class.forName(name);

            if (IStructureUtil.class.isAssignableFrom(decorationClazz))
                structureUtil = (IStructureUtil) decorationClazz.getConstructor().newInstance();

            final Class<?> placingClazz = Class.forName("me.jared.dungeoncrawler." + version + ".block.BlockUtil");

            if (IBlockUtil.class.isAssignableFrom(placingClazz))
                blockUtil = ((IBlockUtil) placingClazz.getConstructor().newInstance());

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        try
        {
            decorationRegistry = new DecorationRegistry("Decorations");
            decorationRegistry.loadDecorations();
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        COMMANDS.setInjector(new SimpleInjector());
        initCommands();

        getServer().getPluginManager().registerEvents(this, this);
    }

    public void onDisable()
    {

    }

    private void initCommands()
    {
        COMMANDS.register(Commands.class);

        getCommand("dungeon").setExecutor(COMMANDS);
    }

    @Override
    public IDecorationRegistry getDecorationRegistry()
    {
        return decorationRegistry;
    }

    @Override
    public IStructureUtil getStructureUtil()
    {
        return structureUtil;
    }

    @Override
    public IBlockUtil getBlockUtil()
    {
        return blockUtil;
    }

    @EventHandler
    public void on(PlayerJoinEvent joinEvent)
    {
        joinEvent.getPlayer().setFlySpeed(.6F);
    }

    @EventHandler
    public void on(PlayerMoveEvent moveEvent)
    {
        Player player = moveEvent.getPlayer();

        Location to = moveEvent.getTo();
        Location from = moveEvent.getFrom();

        double y = to.toVector().subtract(from.toVector()).normalize().getY();

        if (y <= -1 || y >= 1)
        {
            return;
        }

        Vector movement = moveEvent.getFrom().subtract(moveEvent.getTo()).toVector().normalize();

        Vector direction = moveEvent.getPlayer().getLocation().getDirection();

        double dot = movement.dot(direction);

        if (dot < 0) //moving forward
        {

        }
        else //moving backward
        {

        }
    }
}
