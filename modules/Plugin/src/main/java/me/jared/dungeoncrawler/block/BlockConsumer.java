package me.jared.dungeoncrawler.block;

import com.google.common.collect.Lists;
import me.jared.dungeoncrawler.api.data.Tuple;
import me.jared.dungeoncrawler.api.plugin.DungeonCrawler;
import me.jared.dungeoncrawler.api.util.VectorUtil;
import org.bukkit.Location;
import org.bukkit.material.MaterialData;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.List;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;

public class BlockConsumer implements Runnable
{
    private BlockingQueue<Tuple<List<Vector>, MaterialData>> blockingQueue;
    private Location base;
    private boolean isDone = true;

    public BlockConsumer(BlockingQueue<Tuple<List<Vector>, MaterialData>> blockingQueue, Location base)
    {
        this.blockingQueue = blockingQueue;
        this.base = base;
        run();
    }

    @Override
    public void run()
    {
        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                try
                {
                    if (!blockingQueue.isEmpty() && isDone)
                    {
                        isDone = false;

                        Tuple<List<Vector>, MaterialData> dataTuple = blockingQueue.take();

                        List<Vector> vectors = dataTuple.getA();
                        List<Location> locations = VectorUtil.toLocations(vectors, base);

                        Queue<Location> locationQueue = Lists.newLinkedList(locations);

                        new BukkitRunnable()
                        {
                            @Override
                            public void run()
                            {
                                if (locationQueue.isEmpty())
                                {
                                    isDone = true;

                                    DungeonCrawler.getBlockUtil().refreshChunkLocations(locations);

                                    this.cancel();
                                }

                                if (locationQueue.size() < 1000)
                                {
                                    for (int i = 0; i < locationQueue.size(); i++)
                                    {
                                        Location location = locationQueue.poll();

                                        if (location != null)
                                        {
                                            DungeonCrawler.getBlockUtil().setBlockFast(location, dataTuple.getB());
                                        }
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < locationQueue.size() / 3; i++)
                                    {
                                        Location location = locationQueue.poll();

                                        if (location != null)
                                        {
                                            DungeonCrawler.getBlockUtil().setBlockFast(location, dataTuple.getB());
                                        }
                                    }
                                }
                            }
                        }.runTaskTimer(DungeonCrawler.getPlugin(), 0, 5);
                    }
                } catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        }.runTaskTimer(DungeonCrawler.getPlugin(), 0, (long) 2.5);
    }
}
