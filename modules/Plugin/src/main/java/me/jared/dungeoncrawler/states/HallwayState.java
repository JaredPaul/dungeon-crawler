package me.jared.dungeoncrawler.states;

import com.google.common.collect.Lists;
import me.jared.dungeoncrawler.*;
import me.jared.dungeoncrawler.api.data.Tuple;
import me.jared.dungeoncrawler.api.plugin.DungeonCrawler;
import me.jared.dungeoncrawler.api.util.Direction;
import me.jared.dungeoncrawler.cell.Cell;
import me.jared.dungeoncrawler.cell.CellUtil;
import me.jared.dungeoncrawler.delaunay.DT_Point;
import me.jared.dungeoncrawler.delaunay.EdgeList;
import me.jared.dungeoncrawler.pathfinding.AStar;
import me.jared.dungeoncrawler.pathfinding.PathingResult;
import me.jared.dungeoncrawler.pathfinding.Tile;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.material.MaterialData;
import org.bukkit.material.SmoothBrick;
import org.bukkit.material.Wool;
import org.bukkit.util.Vector;

import java.util.List;
import java.util.ListIterator;

public class HallwayState implements State
{
    @Override
    public int order()
    {
        return 10;
    }

    @Override
    public void run(GenerationContext generationContext)
    {
        DungeonCrawler.LOG.info("Generating Hallways...");

        List<EdgeList.Edge> minTree = generationContext.minTree;
        List<Vector> hallways = generationContext.hallways;

        int size = minTree.size();
        while (size > 0)
        {
            EdgeList.Edge edge = minTree.get(size - 1);
            Vector start = getVectorFromDTPoint(edge.getP1());
            Vector end = getVectorFromDTPoint(edge.getP2());

            Cell cell = CellUtil.getCell(start, generationContext.rooms);
            Cell otherCell = CellUtil.getCell(end, generationContext.rooms);

            Tuple<Vector, Vector> closestPoints = CellUtil.getClosestRingPoints(cell, otherCell, 0);

            cell.doorways.add(closestPoints.getA());
            otherCell.doorways.add(closestPoints.getB());

            start = closestPoints.getA();
            end = closestPoints.getB();

            try
            {
                AStar aStar = new AStar(start, end, 5000);

                List<Tile> route = aStar.iterate();

                PathingResult result = aStar.getPathingResult();

                switch (result)
                {
                    case SUCCESS:
                        hallways.addAll(drawPath(route, start, generationContext));
                        break;
                    case NO_PATH:
                        break;
                }
            } catch (AStar.InvalidPathException e)
            {
                e.printStackTrace();
            }

            size--;
        }

        generationContext.setState(new IntersectionState());
        generationContext.runState();
    }

    public List<Vector> drawPath(List<Tile> route, Vector start, GenerationContext context)
    {
        List<Vector> vectorRoute = buildPath(route);
        List<Vector> modifiedRoute = Lists.newArrayList();
        vectorRoute.forEach(vector ->
        {
            Vector worldVector = start.clone().add(vector);

            Cell cell = CellUtil.getCell(worldVector, context.cells);
            if (cell != null)
            {
                if (!context.intersectingCells.contains(cell))
                    context.intersectingCells.add(cell);

                return;
            }

            modifiedRoute.add(worldVector);
        });

        context.drawImmediately(modifiedRoute, new MaterialData(Material.SMOOTH_BRICK));

        return modifiedRoute;
    }

    public List<Vector> buildPath(List<Tile> route)
    {
        List<Vector> vectorRoute = Lists.newArrayList();

        route.forEach(tile -> vectorRoute.add(tile.toVector()));

        ListIterator<Vector> vectorIterator = vectorRoute.listIterator();
        while (vectorIterator.hasNext())
        {
            Vector vector = vectorIterator.next();

            for (int x = -2; x <= 2; x++)
            {
                for (int z = -2; z <= 2; z++)
                {
                    Vector offset = vector.clone().add(new Vector(x, 0, z));
                    vectorIterator.add(offset);
                }
            }
        }

        ListIterator<Vector> edgeIterator = vectorRoute.listIterator();
        while (edgeIterator.hasNext())
        {
            Vector vector = edgeIterator.next();

            for (Direction direction : Direction.getMainDirections())
            {
                Vector check = vector.clone().add(direction.toVector());

                if (!vectorRoute.contains(check))
                {
                    int height = 10;
                    while (height > 0)
                    {
                        edgeIterator.add(check.clone().add(new Vector(0, height, 0)));
                        height--;
                    }
                }
            }
        }

        return vectorRoute;
    }

    private Vector getVectorFromDTPoint(DT_Point p)
    {
        return new Vector((int) p.x(), 0, (int) p.y());
    }
}

