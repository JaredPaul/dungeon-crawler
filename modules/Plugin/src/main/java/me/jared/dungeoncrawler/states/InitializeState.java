package me.jared.dungeoncrawler.states;

import me.jared.dungeoncrawler.delaunay.DisjointSetForest;
import me.jared.dungeoncrawler.GenerationContext;
import org.bukkit.util.Vector;

public class InitializeState implements State
{
    @Override
    public int order()
    {
        return 0;
    }

    @Override
    public void run(GenerationContext generationContext)
    {
        generationContext.baseVector = new Vector();
        generationContext.forest = new DisjointSetForest<>();
        generationContext.random = generationContext.getRandom();

        generationContext.setState(new GenerateCellsState());
        generationContext.runState();
    }
}
