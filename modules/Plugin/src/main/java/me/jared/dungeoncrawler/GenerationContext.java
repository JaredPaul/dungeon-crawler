package me.jared.dungeoncrawler;

import com.google.common.collect.Lists;
import com.google.common.collect.Queues;
import me.jared.dungeoncrawler.api.data.Tuple;
import me.jared.dungeoncrawler.api.plugin.DungeonCrawler;
import me.jared.dungeoncrawler.api.util.VectorUtil;
import me.jared.dungeoncrawler.block.BlockConsumer;
import me.jared.dungeoncrawler.cell.Cell;
import me.jared.dungeoncrawler.cell.RoomConnection;
import me.jared.dungeoncrawler.delaunay.DT_Point;
import me.jared.dungeoncrawler.delaunay.DelaunayTriangulation;
import me.jared.dungeoncrawler.delaunay.DisjointSetForest;
import me.jared.dungeoncrawler.delaunay.EdgeList;
import me.jared.dungeoncrawler.states.InitializeState;
import me.jared.dungeoncrawler.states.State;
import me.jared.dungeoncrawler.util.XORShiftRandom;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.material.MaterialData;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadLocalRandom;

public class GenerationContext
{
    public Map map;

    public final BlockingQueue<Tuple<List<Vector>, MaterialData>> BLOCK_PLACER = Queues.newLinkedBlockingQueue();

    public Location base;

    public Vector areaSize = new Vector(256 + 128, 0, 192);
    public Vector areaCenter = new Vector(areaSize.getBlockX() >> 1, 0, areaSize.getBlockZ() >> 1);
    public Vector cellSize = new Vector(1, 0, 12);
    public int radius = 16;

    public int minRooms = 4;
    public int roomAreaThreshold = 42;

    public int cellCount;
    public List<Cell> cells = Lists.newArrayList();
    public List<Cell> fillerCells = Lists.newArrayList();
    public List<Cell> cellsMaster = Lists.newArrayList();
    public List<Cell> rooms = Lists.newArrayList();
    public List<Cell> intersectingCells = Lists.newArrayList();

    public List<RoomConnection> roomConnections = Lists.newArrayList();
    public List<Vector> hallways = Lists.newArrayList();

    public DelaunayTriangulation delaunayTriangulation;

    //minimum spanning tree
    public List<EdgeList.Edge> minTree = Lists.newArrayList();
    public List<EdgeList.Edge> edges = Lists.newArrayList();
    public List<EdgeList.Edge> discardedEdges = Lists.newArrayList();

    public DisjointSetForest<DT_Point> forest;

    public Vector baseVector;

    public XORShiftRandom random;

    private State state;

    public GenerationContext(Location base)
    {
        this.base = base;
        new BlockConsumer(BLOCK_PLACER, base);
        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                setState(new InitializeState());
                runState();
            }
        }.runTaskAsynchronously(DungeonCrawler.getPlugin());

    }

    public void setState(State state)
    {
        this.state = state;
    }

    public State getState()
    {
        return state;
    }

    public void runState()
    {
        state.run(this);
    }

    public XORShiftRandom getRandom()
    {
        long seed = ThreadLocalRandom.current().nextLong();

        return new XORShiftRandom(seed);
    }

    public void drawImmediately(List<Vector> vectors, MaterialData materialData)
    {
        drawImmediately(new Tuple<>(vectors, materialData));
    }

    public void drawImmediately(Tuple<List<Vector>, MaterialData> dataTuple)
    {
        List<Vector> vectors = dataTuple.getA();
        List<Location> locations = VectorUtil.toLocations(vectors, base);

        MaterialData data = dataTuple.getB();

        for (Location location : locations)
        {
            Bukkit.getScheduler().runTask(DungeonCrawler.getPlugin(), () ->
                    DungeonCrawler.getBlockUtil().setBlockFast(location, data.getItemType(), data.getData()));
        }

        Bukkit.getScheduler().runTask(DungeonCrawler.getPlugin(), () ->
                DungeonCrawler.getBlockUtil().refreshChunkLocations(locations));
    }

    public void drawInQueue(List<Vector> vectors, MaterialData data)
    {
        drawInQueue(new Tuple<>(vectors, data));
    }

    public void drawInQueue(Tuple<List<Vector>, MaterialData> dataTuple)
    {
        try
        {
            BLOCK_PLACER.put(dataTuple);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
