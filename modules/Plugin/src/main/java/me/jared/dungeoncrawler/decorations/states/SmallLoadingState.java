package me.jared.dungeoncrawler.decorations.states;

import me.jared.dungeoncrawler.GenerationContext;
import me.jared.dungeoncrawler.api.decorations.IDecoration;
import me.jared.dungeoncrawler.decorations.DecorationLoader;

public class SmallLoadingState extends AbstractLoadingState
{
    public SmallLoadingState()
    {
        super(IDecoration.Size.SMALL, 4);
    }

    @Override
    public void run(GenerationContext context, DecorationLoader decorationLoader)
    {
        super.run(context, decorationLoader);

        decorationLoader.setState(null);
    }
}
