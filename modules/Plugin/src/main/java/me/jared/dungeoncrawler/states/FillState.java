package me.jared.dungeoncrawler.states;

import me.jared.dungeoncrawler.*;
import me.jared.dungeoncrawler.api.plugin.DungeonCrawler;
import me.jared.dungeoncrawler.api.structures.AABB;
import me.jared.dungeoncrawler.cell.Cell;
import me.jared.dungeoncrawler.cell.CellType;
import me.jared.dungeoncrawler.cell.CellUtil;
import org.bukkit.util.Vector;

import java.util.List;

public class FillState implements State
{
    @Override
    public int order()
    {
        return 3;
    }

    @Override
    public void run(GenerationContext generationContext)
    {
        DungeonCrawler.LOG.info("Filling Map...");

        List<Cell> cells = generationContext.cells;
        List<Cell> fillerCells = generationContext.fillerCells;

        Map map = generationContext.map = Map.get(cells);

        Vector baseVector = generationContext.baseVector;

        for (double x = map.getLeft(); x < map.getRight(); x++)
        {
            for (double z = map.getBottom(); z < map.getTop(); z++)
            {
                baseVector.setX(x);
                baseVector.setZ(z);

                if (CellUtil.getCell(baseVector, cells) == null)
                {
                    Cell cell = new Cell();
                    cell.setAABB(new AABB(x, baseVector.getBlockY(), z, x, baseVector.getBlockY(), z));
                    cell.type = CellType.FILL;
                    fillerCells.add(cell);
                }
            }
        }

        generationContext.setState(new SelectRoomsState());
        generationContext.runState();
    }
}
