package me.jared.dungeoncrawler.decorations.states;

import me.jared.dungeoncrawler.GenerationContext;
import me.jared.dungeoncrawler.api.decorations.IDecoration;
import me.jared.dungeoncrawler.decorations.DecorationLoader;

public class BigLoadingState extends AbstractLoadingState
{
    public BigLoadingState()
    {
        super(IDecoration.Size.BIG, 1);
    }

    @Override
    public void run(GenerationContext context, DecorationLoader decorationLoader)
    {
        super.run(context, decorationLoader);

        decorationLoader.setState(new MediumLoadingState());
        decorationLoader.run();
    }
}
