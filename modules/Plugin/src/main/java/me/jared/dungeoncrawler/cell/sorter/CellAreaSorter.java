package me.jared.dungeoncrawler.cell.sorter;

import me.jared.dungeoncrawler.cell.Cell;

import java.util.Comparator;

public class CellAreaSorter implements Comparator<Cell>
{
    @Override
    public int compare(Cell cell1, Cell cell2)
    {
        if (cell1.getArea() < cell2.getArea())
        {
            return 1;
        }
        else if (cell1.getArea() > cell2.getArea())
        {
            return -1;
        }

        return 0;
    }
}
