package me.jared.dungeoncrawler.states;

import me.jared.dungeoncrawler.delaunay.DisjointSetForest;
import me.jared.dungeoncrawler.delaunay.EdgeList;
import me.jared.dungeoncrawler.GenerationContext;
import me.jared.dungeoncrawler.delaunay.DT_Point;

import java.util.List;

public class MinimumSpanningState implements State
{
    @Override
    public int order()
    {
        return 6;
    }

    @Override
    public void run(GenerationContext generationContext)
    {
        List<EdgeList.Edge> edges = generationContext.edges;
        List<EdgeList.Edge> minTree = generationContext.minTree;
        List<EdgeList.Edge> discardedEdges = generationContext.discardedEdges;
        DisjointSetForest<DT_Point> forest = generationContext.forest;


        while (edges.size() > 0)
        {
            EdgeList.Edge edge = edges.remove(edges.size() - 1);

            if (!forest.findSet(edge.getNode1()).equals(forest.findSet(edge.getNode2())))
            {
                minTree.add(edge);
                forest.union(edge.getNode1(), edge.getNode2());
            }
            else
            {
                discardedEdges.add(edge);
            }
        }

        generationContext.setState(new HallwayState());
        generationContext.runState();
    }
}
