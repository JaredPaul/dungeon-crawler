package me.jared.dungeoncrawler.cell;

public enum CellType
{
    OVERLAP(-1),
    NONE(0),
    ROOM(1),
    FILL(2);

    private int id;

    CellType(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }
}
