package me.jared.dungeoncrawler.states;

import me.jared.dungeoncrawler.GenerationContext;
import me.jared.dungeoncrawler.api.plugin.DungeonCrawler;
import me.jared.dungeoncrawler.decorations.DecorationLoader;

public class DecorationState implements State
{

    @Override
    public int order()
    {
        return 12;
    }

    @Override
    public void run(GenerationContext generationContext)
    {
        DungeonCrawler.LOG.info("Loading Decorations...");

        //fills all the cells (rooms) with decorations
        DecorationLoader decorationLoader = new DecorationLoader(generationContext);
        decorationLoader.run();
    }
}
