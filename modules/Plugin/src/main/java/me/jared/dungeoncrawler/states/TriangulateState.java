package me.jared.dungeoncrawler.states;

import me.jared.dungeoncrawler.cell.Cell;
import me.jared.dungeoncrawler.delaunay.EdgeList;
import me.jared.dungeoncrawler.GenerationContext;
import me.jared.dungeoncrawler.api.plugin.DungeonCrawler;
import me.jared.dungeoncrawler.delaunay.DT_Point;
import me.jared.dungeoncrawler.delaunay.DelaunayTriangulation;

import java.util.List;

public class TriangulateState implements State
{
    @Override
    public int order()
    {
        return 5;
    }

    @Override
    public void run(GenerationContext generationContext)
    {
        DungeonCrawler.LOG.info("Connecting Rooms...");

        DelaunayTriangulation delaunayTriangulation = generationContext.delaunayTriangulation = new DelaunayTriangulation();
        List<Cell> rooms = generationContext.rooms;

        for (Cell room : rooms)
        {
            delaunayTriangulation.insertPoint(new DT_Point(room.getCenter().getBlockX(), room.getCenter().getBlockZ()));
        }

        generationContext.edges = new EdgeList(delaunayTriangulation, generationContext.forest).getList();

        generationContext.setState(new MinimumSpanningState());
        generationContext.runState();
    }
}
