package me.jared.dungeoncrawler.cell.sorter;

import me.jared.dungeoncrawler.cell.Cell;

import java.util.Comparator;

public class Strategies
{
    private static final Comparator<Cell> CELL_AREA_COMPARATOR = new CellAreaSorter();

    private static final Comparator<Cell> CELL_DISTANCE_COMPARATOR = new CellDistanceSorter();

    public static Comparator<Cell> getCellAreaComparator()
    {
        return CELL_AREA_COMPARATOR;
    }

    public static Comparator<Cell> getCellDistanceComparator()
    {
        return CELL_DISTANCE_COMPARATOR;
    }
}
