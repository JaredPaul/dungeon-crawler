package me.jared.dungeoncrawler.v1_12_R1.block;

import me.jared.dungeoncrawler.api.block.AbstractMaterialAndData;
import org.bukkit.Material;

public class MaterialAndData extends AbstractMaterialAndData
{
    private byte legacyData;

    public MaterialAndData(Material material, byte legacyData)
    {
        super(material);
        this.legacyData = legacyData;
    }

    @Override
    public String getBlockData()
    {
        return null;
    }

    @Override
    public byte getBlockDataLegacy()
    {
        return legacyData;
    }
}
