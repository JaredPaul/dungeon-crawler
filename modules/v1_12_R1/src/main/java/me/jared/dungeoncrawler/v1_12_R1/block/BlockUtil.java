package me.jared.dungeoncrawler.v1_12_R1.block;

import com.google.common.collect.Lists;
import me.jared.dungeoncrawler.api.block.IBlockUtil;
import me.jared.dungeoncrawler.api.block.IMaterialAndData;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.material.MaterialData;

import java.util.List;

public class BlockUtil implements IBlockUtil
{
    @Override
    public boolean setBlockFast(Location location, Material material, byte legacyData)
    {
        return setBlockFast(location, new MaterialAndData(material, legacyData));
    }

    @Override
    public boolean setBlockFast(Location location, IMaterialAndData materialAndData)
    {
        Material material = materialAndData.getMaterial();
        byte legacyData = materialAndData.getBlockDataLegacy();

        return setBlockFast(location, new MaterialData(material, legacyData));
    }

    @Override
    public boolean setBlockFast(Location location, MaterialData data)
    {
        return setBlockFast(location.getWorld(), location.getBlockX(), location.getBlockY(), location.getBlockZ(), data);
    }

    public boolean setBlockFast(World world, int x, int y, int z, MaterialData data)
    {
        Block block = world.getBlockAt(x, y, z);
        BlockState state = block.getState();
        state.setType(data.getItemType());
        state.setData(data);
        state.update(true);
        return true;
    }

    @Override
    public void refreshChunkLocations(List<Location> locations)
    {
        refreshChunks(getChunks(locations));
    }

    @Override
    public void refreshChunks(List<Chunk> chunks)
    {
        chunks.forEach(chunk -> chunk.getWorld().refreshChunk(chunk.getX(), chunk.getZ()));
    }

    @Override
    public List<Chunk> getChunks(List<Location> locations)
    {
        List<Chunk> chunks = Lists.newArrayList();

        for (Location location : locations)
        {
            Chunk chunk = location.getChunk();

            if (!chunks.contains(chunk))
                chunks.add(chunk);
        }

        return chunks;
    }
}
